def my_name():
    print("Tomasz")


def suma3(liczba_1, liczba_2, liczba_3):
    return liczba_1 + liczba_2 + liczba_3


def hello_world():
    print("Cześć Świecie")


def roznica():
    print(10-100)


def roznica2(liczba1, liczba2):
    return liczba1-liczba2


class Hammer:
    pass


def use_hammer():
    return Hammer()


my_name()
print(suma3(1, 2, 3))
hello_world()
roznica()
print(roznica2(10, 5))
tool = use_hammer()
print(tool)

# Second part of homework

class School:
    def __init__(self, name, no):
        self.name = name
        self.no = no
        print("PRIMARY SCHOOL, named: {}, number: {}".format(self.name, self.no))

    def __del__(self):
        print("i to by było na tyle...")


class PublicSchool(School):
    pass


school1 = PublicSchool("no name", "no number")
print(school1)

